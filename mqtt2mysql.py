# Copyright 2024, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the
#       distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Connect to a database and an MQTT broker and persist received messages. The
MQTT subscriptions are specified in the database.
"""

__version__ = "0.0.1"

from argparse import ArgumentParser
import json
import sys

import mysql.connector
import paho.mqtt.client as mqtt

class Connection:
    """
    Wrap a MySQL connection as a context manager.
    """

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def __enter__(self):
        self.conn = mysql.connector.connect(*self.args, **self.kwargs)
        return self.conn

    def __exit__(self, type, val, tb):
        self.conn.close()
        self.conn = None

class Cursor:
    """
    Wrap a MySQL cursor as a context manager.
    """

    def __init__(self, connection, *args, **kwargs):
        self.connection = connection
        self.args = args
        self.kwargs = kwargs

    def __enter__(self):
        self.cursor = self.connection.cursor(*self.args, **self.kwargs)
        return self.cursor

    def __exit__(self, type, val, tb):
        self.cursor.close()
        self.cursor = None

class Inserter:
    """
    Receive a database connection and insert MQTT messages into the database.
    """

    def __init__(self, subscription, db):
        """
        The subscription specifies the user, host and port of the originating
        MQTT broker, the topic to subscribe to and the object and
        subscription_id to insert into the database.

        Messages will be inserted into the given database.
        """
        self.db = db
        self.subscription = subscription
        self.cursor = db.cursor()

    @property
    def broker(self):
        """
        The broker to which we are connected.
        """
        return "{}@{}:{}".format(
            self.subscription.user,
            self.subscription.host,
            self.subscription.port
        )

    @property
    def topic(self):
        """
        The topic to which we should subscribe.
        """
        return self.subscription.topic

    def insert(self, topic, payload):
        """
        Read the object from the payload and insert its attributes into the
        messages table in the database.
        """
        for field, value in payload[self.subscription.object].items():
            self.cursor.execute(
                """
                insert into messages(subscription_id, time, topic, field, value)
                    values (
                        %(subscription_id)s,
                        %(time)s,
                        %(topic)s,
                        %(field)s,
                        %(value)s
                    )
                """,
                {
                    "subscription_id": self.subscription.subscription_id,
                    "time": payload["Time"],
                    "topic": topic,
                    "field": field,
                    "value": value
                }
            )
        self.db.commit()

def on_connect(client, inserter, flags, rc):
    if 0 != rc:
        raise RuntimeError("{}: {}".format(
            inserter.broker, mqtt.error_string(rc)
        ))
    client.subscribe(inserter.topic)

def on_message(client, inserter, message):
    inserter.insert(message.topic, json.loads(message.payload.decode("utf8")))

def parse_args():
    """
    Parse command line arguments and return the namespace.
    """

    parser = ArgumentParser(description=sys.modules[__name__].__doc__)
    parser.add_argument(
        "-d", "--database",
        default="mqtt2mysql",
        help="Database name (default %(default)s)"
    )
    parser.add_argument(
        "-P", "--port",
        default=3306,
        help="Database port number (default %(default)s)",
        type=int
    )
    parser.add_argument("-p", "--password", help="Database user password")
    parser.add_argument(
        "-u", "--user",
        default="mqtt2mysql",
        help="Database user name (default %(default)s)"
    )
    parser.add_argument(
        "-V", "--version",
        action="version",
        version="%(prog)s {}".format(sys.modules[__name__].__version__)
    )
    parser.add_argument(
        "host",
        default="127.0.0.1",
        help="MySQL server (default %(default)s)",
        nargs="?"
    )
    return parser.parse_args()

def main():
    """
    Parse arguments, connect to the database and perform the specified
    subscriptions.
    """

    args = parse_args()
    with Connection(
        database=args.database,
        host=args.host,
        password=args.password,
        port=args.port,
        raise_on_warnings=True,
        user=args.user
    ) as conn:
        clients = []
        with Cursor(conn, named_tuple=True) as cursor:
            cursor.execute(
                "select * from brokers join subscriptions using (broker_id)"
            )
            for row in cursor:
                client = mqtt.Client(userdata=Inserter(row, conn))
                client.username_pw_set(row.user or "mqtt2mysql", row.password)
                client.connect_async(
                    row.host or "localhost",
                    port=row.port or 1883
                )
                client.on_connect = on_connect
                client.on_message = on_message
                clients.append(client)

        while True:
            for client in clients:
                client.loop_forever()  # FIXME
