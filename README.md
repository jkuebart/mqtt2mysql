<!--
 - Copyright 2024, Joachim Kuebart <joachim.kuebart@gmail.com>
 -
 - Redistribution and use in source and binary forms, with or without
 - modification, are permitted provided that the following conditions are
 - met:
 -
 -   1. Redistributions of source code must retain the above copyright
 -      notice, this list of conditions and the following disclaimer.
 -
 -   2. Redistributions in binary form must reproduce the above copyright
 -      notice, this list of conditions and the following disclaimer in the
 -      documentation and/or other materials provided with the
 -      distribution.
 -
 -   3. Neither the name of the copyright holder nor the names of its
 -      contributors may be used to endorse or promote products derived
 -      from this software without specific prior written permission.
 -
 - THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 - IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 - TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 - PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 - HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 - SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 - TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 - PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 - LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 - NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 - SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-->

MQTT2MySQL
==========

Yet another script to persist MQTT messages in a MySQL database.


Installation
------------

This software can be installed using

    pip install git+https://gitlab.com/jkuebart/mqtt2mysql.git


Usage
-----

    usage: mqtt2mysql [-h] [-d DATABASE] [-P PORT] [-p PASSWORD] [-u USER]
                      [-V] [host]

    Connect to a database and an MQTT broker and persist received messages.
    The MQTT subscriptions are specified in the database.

    positional arguments:
      host                  MySQL server (default 127.0.0.1)

    optional arguments:
      -h, --help            show this help message and exit
      -d DATABASE, --database DATABASE
                            Database name (default mqtt2mysql)
      -P PORT, --port PORT  Database port number (default 3306)
      -p PASSWORD, --password PASSWORD
                            Database user password
      -u USER, --user USER  Database user name (default mqtt2mysql)
      -V, --version         show program's version number and exit

The program connects to the specified database (or `"mqtt2mysql"`) on the
given database host (or `"localhost"`). It expects the following tables to
contain the desired MQTT subscriptions:

```sql
create table brokers(
    broker_id int(11) not null auto_increment primary key,
    host varchar(255),
    port int(5),
    user varchar(255),
    password varchar(255)
);

create table subscriptions(
    subscription_id int(11) not null auto_increment primary key,
    broker_id int(11) not null references brokers(broker_id),
    topic varchar(255),
    object varchar(255)
);
```

The specified `topic` is subscribed to on the given MQTT broker. If
unspecified, `"localhost"` is used for `host`, `1883` for `port` and
`"mqtt2mysql"` for `user`.

The following table will receive one or more rows per message:

```sql
create table messages(
    message_id int(11) not null auto_increment primary key,
    subscription_id int(11) not null
        references subscriptions(subscription_id),
    time timestamp,
    topic varchar(255),
    field varchar(255),
    value decimal(12,1)
);
```

The field/value pairs of the message payload object given by `object` are
added to the `messages` table.

The required tables can be created by running
[`create.sql`](data/create.sql).

    mysql mqtt2mysql <data/create.sql


Example
-------

Given the following MQTT message payload:

```json
{
    "Power": {
        "active_instantaneous": 181,
        "active_positive_total": 1823937.3
    },
    "Time": "2024-01-08T16:57:42"
}
```

If `subscriptions.object` is set to `"Power"`, two rows are added to the
table `messages` for the `"active_instantaneous"` and
`"active_positive_total"` values, respectively. Furthermore, the value of
the `"Time"` field is entered into the `messages.time` column.
